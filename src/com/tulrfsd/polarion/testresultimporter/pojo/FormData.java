package com.tulrfsd.polarion.testresultimporter.pojo;

import org.apache.commons.fileupload.FileItem;

public class FormData {

  FileItem xmlFile;
  String token;
  String username;
  String password;

  public FormData() {
    xmlFile = null;
    token = "";
    username = "";
    password = "";
  }

  public FileItem getXmlFile() {
    return xmlFile;
  }

  public void setXmlFile(FileItem xmlFile) {
    this.xmlFile = xmlFile;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }
}
