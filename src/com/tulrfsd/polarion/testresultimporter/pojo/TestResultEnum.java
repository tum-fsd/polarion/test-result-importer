package com.tulrfsd.polarion.testresultimporter.pojo;

import com.tulrfsd.polarion.testresultimporter.servlet.Settings;

/**
 * Definition of the supported test result verdicts.
 */
public enum TestResultEnum {
  PASSED(Settings.TEST_RESULT_ID_PASSED),
  PARTIAL(Settings.TEST_RESULT_ID_PARTIAL),
  FAILED(Settings.TEST_RESULT_ID_FAILED),
  BLOCKED(Settings.TEST_RESULT_ID_BLOCKED);

  private final String id;

  private TestResultEnum(String polarionEnumOptionId) {
    this.id = polarionEnumOptionId;
  }

  public String getId() {
    return this.id;
  }
}
