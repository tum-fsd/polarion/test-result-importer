package com.tulrfsd.polarion.testresultimporter.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.security.Principal;
import java.security.PrivilegedAction;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import javax.security.auth.Subject;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import org.apache.commons.io.IOUtils;
import com.polarion.alm.projects.model.IUser;
import com.polarion.alm.tracker.ITestManagementService;
import com.polarion.alm.tracker.ITrackerService;
import com.polarion.alm.tracker.model.ITestRecord;
import com.polarion.alm.tracker.model.ITestRun;
import com.polarion.alm.tracker.model.IWorkflowObject;
import com.polarion.core.util.RunnableWEx;
import com.polarion.core.util.types.Text;
import com.polarion.platform.TransactionExecuter;
import com.polarion.platform.internal.security.UserPrincipal;
import com.polarion.platform.persistence.IEnumOption;
import com.polarion.platform.persistence.IEnumeration;
import com.polarion.platform.security.AuthenticationFailedException;
import com.polarion.platform.security.ISecurityService;
import com.polarion.platform.security.login.AccessToken;
import com.polarion.platform.security.login.IPassword;
import com.polarion.platform.security.login.IToken;
import com.polarion.platform.security.login.Password;
import com.polarion.subterra.base.data.model.IEnumType;
import com.tulrfsd.polarion.core.utils.ServicesProvider;
import com.tulrfsd.polarion.core.utils.WorkflowObjectCoreHelper;
import com.tulrfsd.polarion.testresultimporter.pojo.TestCaseData;
import com.tulrfsd.polarion.testresultimporter.pojo.TestRunData;
import com.tulrfsd.polarion.testresultimporter.pojo.TestRunWrapper;

/** Performs the import of the test results specific to the data model of the xml file. */
public class TestResultImporter implements Runnable {
  
  public static Subject getUserSubject(String token, String username, String password) throws AuthenticationFailedException {
    ISecurityService securityService = ServicesProvider.getSecurityService();
    
    if (!token.isEmpty()) {
      IToken<AccessToken> accessToken = AccessToken.token(token);
      return securityService.login().from("default").authenticator(AccessToken.id()).with(accessToken).perform();
    } else {
      IPassword<Password> credentials = Password.of(username, password);
      return securityService.login().from("default").authenticator(Password.id("polarion_login_form")).with(credentials).perform();
    }
  }

  private final ITrackerService trackerService;
  private final ITestManagementService tmService;
  private final List<TestRunData> testRuns;
  private IUser user;

  public TestResultImporter(InputStream fileStream) throws JAXBException, IOException {
    this.trackerService = ServicesProvider.getTrackerService();
    this.tmService = ServicesProvider.getTestManagementService();
    this.testRuns = getObjectFromXml(TestRunWrapper.class, fileStream);
  }

  /** Imports the test results as the specified user. */
  public void importAs(Subject userSubject) {
    ISecurityService securitySerivce = ServicesProvider.getSecurityService();
    
    for (Principal principal : userSubject.getPrincipals()) {
      if (principal instanceof UserPrincipal userPrincipal) {
        this.user = ServicesProvider.getProjectService().getUser(userPrincipal.getName());
      }
    }
    if (this.user == null) {
      throw new IllegalArgumentException("Could not find username in subject principal.");
    }
    
    try {
      securitySerivce.doAsUser(userSubject, new PrivilegedAction<Object>() {
        @SuppressWarnings("unchecked")
        public Object run() {
          return TransactionExecuter.execute(RunnableWEx.wrap(TestResultImporter.this));
        }
      });
    } finally {
      securitySerivce.logout(userSubject);
    }
  }
  

  @Override
  public void run() throws RuntimeException {
    if (this.testRuns.isEmpty()) {
      throw new IllegalArgumentException("The uploaded file does not contain any test runs.");
    }
    validateProjectsExist();
    validateAllTestCasesExist();
    validateTestRunTemplateExists();

    for (TestRunData testRun : this.testRuns) {
      createTestRun(testRun);
    }
  }

  private List<TestRunData> getObjectFromXml(Class<TestRunWrapper> mainClass, InputStream xmlStream) throws JAXBException, IOException {    
    if (xmlStream == null) {
      return Collections.emptyList();
    }
    Unmarshaller jaxbUnmarshaller = JAXBContext.newInstance(mainClass).createUnmarshaller();
    return ((TestRunWrapper) jaxbUnmarshaller.unmarshal(new StringReader(IOUtils.toString(xmlStream, StandardCharsets.UTF_8)))).getTestRuns();
  }

  private void validateProjectsExist() {
    testRuns.stream()
            .map(TestRunData::getProjectId)
            .filter(projectId -> this.trackerService.getTrackerProject(projectId).isUnresolvable())
            .findAny()
            .ifPresent(projectId -> {
              throw new IllegalArgumentException(String.format("The project with the ID %s does not exist or the user does not have access.", projectId));
            });
  }

  private void validateAllTestCasesExist() {
    for (TestRunData testRunData : testRuns) {
      for (TestCaseData testCaseData : testRunData.getTestCaseDataSet()) {
        // if the test case has a projectId specified, use that instead of the projectId for the test run.
        String projectId = testCaseData.getProjectId().isEmpty() ? testRunData.getProjectId() : testCaseData.getProjectId();
        if (this.trackerService.getWorkItemWithRevision(projectId, testCaseData.getWorkItemId(), testCaseData.getWorkItemRevision()).isUnresolvable()) {
          throw new IllegalArgumentException(String.format("The test case with the ID %s does not exist in the project \"%s\" for revision %s or the user does not have access.", testCaseData.getWorkItemId(),
              this.trackerService.getTrackerProject(projectId).getName(), testCaseData.getWorkItemRevision()));
        }
      }
    }
  }

  private void validateTestRunTemplateExists() {
    for (TestRunData testRunData : testRuns) {
      String query = String.format("project.id:%s AND id:%s", testRunData.getProjectId(), testRunData.getTestRunTemplateId());
      if (this.tmService.searchTestRunTemplates(query, "project.id", -1).isEmpty()) {
        throw new IllegalArgumentException(String.format("The test run template %s does not exist in the project \"%s\" or the user does not have access.",
            testRunData.getTestRunTemplateId(), this.trackerService.getTrackerProject(testRunData.getProjectId()).getName()));
      }
    }
  }

  private void createTestRun(TestRunData testRunData) {
    String testRunId = testRunData.getFormattedExecutionDate();
    if (!this.tmService.getTestRun(testRunData.getProjectId(), testRunId).isUnresolvable()) {
      throw new IllegalArgumentException(String.format("The test run with the ID \"%s\" already exists in the project \"%s\". No changes were made.",
          testRunId, this.trackerService.getTrackerProject(testRunData.getProjectId()).getName()));
    }
    ITestRun testRun = tmService.createTestRun(testRunData.getProjectId(), testRunId, testRunData.getTestRunTemplateId());
    setFieldValues(testRun, testRunData);
    setTestRecords(testRun, testRunData);
    testRun.setFinishedOn(testRunData.getExecutionDate());
    
    if (testRun.getRecordsCount() == testRun.getRecordsCount(Settings.TEST_RESULT_ID_PASSED)) {
      testRun.setEnumerationValue(IWorkflowObject.KEY_STATUS, Settings.TEST_RUN_STATUS_ID_PASSED);
    } else {
      testRun.setEnumerationValue(IWorkflowObject.KEY_STATUS, Settings.TEST_RUN_STATUS_ID_FAILED);
      if (Settings.CREATE_SUMMARY_DEFECT) {
        testRun.createSummaryDefect(null);
      }
    }
    testRun.save();
  }

  private void setTestRecords(ITestRun testRun, TestRunData testRunData) {
    for (TestCaseData testCaseData : testRunData.getTestCaseDataSet()) {
      ITestRecord testRecord = testRun.addRecord();
      
      // if the test case has a projectId specified, use that instead of the projectId for the test run.
      String projectId;
      if (testCaseData.getProjectId().isEmpty()) {
        projectId = testRunData.getProjectId();
      } else {
        projectId = testCaseData.getProjectId();
      }
      testRecord.setTestCaseWithRevision(this.trackerService.getWorkItemWithRevision(projectId, testCaseData.getWorkItemId(), testCaseData.getWorkItemRevision()));
      
      if (!testCaseData.getExternalTestCaseURI().isEmpty()) {
        testRecord.setValue(Settings.TEST_RECORD_FIELD_ID_EXTERNAL_TEST_CASE_URI, testCaseData.getExternalTestCaseURI());
      }
      if (!testCaseData.getSummary().isEmpty()) {
        testRecord.setComment(Text.html(testCaseData.getSummary()));
      }
      testRecord.setResult(testCaseData.getResult().getId());
      testRecord.setExecuted(testRunData.getExecutionDate());
      testRecord.setExecutedBy(this.user);
    }
  }

  private void setFieldValues(ITestRun testRun, TestRunData testRunData) {
    if (testRunData.getTitle().isEmpty()) {
      testRun.setTitle(testRunData.getSystemUnderTestCategory() + (testRunData.hasSimplifiedConditions() ? " (simplified)" : ""));
    } else {
      testRun.setTitle(testRunData.getTitle());
    }
    
    if (testRunData.getBaseline() != null) {
      testRun.setValue(Settings.TEST_RUN_FIELD_ID_BASELINE, testRunData.getBaseline());
    }
    
    testRun.setKeepInHistory(testRunData.getKeepInHistory());

    if (!testRunData.getSystemUnderTestCategory().isEmpty()) {
      setSystemUnderTestCategory(testRun, testRunData.getSystemUnderTestCategory());
    }
    if (!testRunData.getSystemUnderTestURI().isEmpty()) {
      testRun.setValue(Settings.TEST_RUN_FIELD_ID_SYSTEM_UNDER_TEST_URI,testRunData.getSystemUnderTestURI());
    }
    if(!testRunData.getDetailedResultsURI().isEmpty()) {
      testRun.setValue(Settings.TEST_RUN_FIELD_ID_DETAILED_RESULTS_URI, testRunData.getDetailedResultsURI());
    }
    if (testRunData.hasSimplifiedConditions()) {
      testRun.setValue(Settings.TEST_RUN_FIELD_ID_SIMPLIFIED_CONDITIONS, true);
    }
    if (!testRunData.getEnvironmentDescription().isEmpty()) {
      testRun.setValue(Settings.TEST_RUN_FIELD_ID_ENVIRONMENT_DESCRIPTION, Text.plain(testRunData.getEnvironmentDescription()));
    }
  }
  
  private void setSystemUnderTestCategory(ITestRun testRun, String systemUnderTestCategory) {
    Optional<IEnumType> enumType = testRun.getEnumerationTypeForField(Settings.TEST_RUN_FIELD_ID_SYSTEM_UNDER_TEST_CATEGORY);
    if (!enumType.isPresent()) {
      throw new IllegalArgumentException(String.format("The custom field with the ID %s is not an enum field for the test run type \"%s\".",
          Settings.TEST_RUN_FIELD_ID_SYSTEM_UNDER_TEST_CATEGORY, testRun.getType().getName()));
    }
    @SuppressWarnings("unchecked")
    IEnumeration<IEnumOption> categoryEnum = testRun.getDataSvc().getEnumerationForEnumId(enumType.get(), testRun.getContextId());
    IEnumOption enumOption = categoryEnum.wrapOption(systemUnderTestCategory);
    if (enumOption.isPhantom()) {
      throw new IllegalArgumentException(String.format("The enumeration field %s does not have the enum option %s. No changes were made.",
          testRun.getFieldLabel(Settings.TEST_RUN_FIELD_ID_SYSTEM_UNDER_TEST_CATEGORY), systemUnderTestCategory));
    }
    WorkflowObjectCoreHelper.setEnumOption(testRun, Settings.TEST_RUN_FIELD_ID_SYSTEM_UNDER_TEST_CATEGORY, enumOption, true);
  }

}
