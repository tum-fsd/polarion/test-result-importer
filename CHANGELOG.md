# Changelog

## 6.1.0

### Features

- **BREAKING CHANGE**: cleaned up format of the xml file without backward compatibility
- added support to optionally create a summary defect for the test run if at least one test failed

### Chores

- code cleanup

## 5.2.0

### Features

- set test run status to passed if all test cased passed, and failed otherwise
- support for custom String field in test record to point to external test case
- updated xml file structure with backward compatibility (check the xsd for the new field names)
- make the xsd schema file available at `<Polarion Base URL>/polarion/testresultimporter/TestResultsSchema.xsd` for direct validation

### Fixes

- correctly display error messages in browser

### Chores

- updated documentation

## 5.1.0

### Features

- added support for optional custom title
- added support for optional environment description
- added support for unordered elements in the TestRunData and TestCaseData xml types

### Chores

- code cleanup
- updated dependencies

## 5.0.1

### Chores

- compatibility fixes for Polarion 2404

## 5.0.0

- initial public release
- supports authentication via username/password or personal access token