package com.tulrfsd.polarion.testresultimporter.pojo;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Nodes for a test run in the xml results file.
 */
@XmlRootElement(name = "testRun")
@XmlAccessorType(XmlAccessType.FIELD)
public class TestRunData {
  
  @XmlElement(required = true)
  String projectId;
  
  @XmlElement(required = true)
  String testRunTemplateId;
  
  @XmlElement(required = false)
  String title;
  
  @XmlElement(required = false)
  Integer baseline;

  @XmlElement(required = false)
  Boolean keepInHistory;

  @XmlElement(required = true)
  Date executionDate;

  @XmlElement(required = false)
  String systemUnderTestCategory;

  @XmlElement(required = false)
  String systemUnderTestURI;
  
  @XmlElement(required = false)
  String detailedResultsURI;

  @XmlElement(required = false, name = "simplifiedConditions")
  Boolean hasSimplifiedConditions;
  
  @XmlElement(required = false)
  String environmentDescription;

  @XmlElementWrapper(name = "testCases", required = true)
  @XmlElement(name = "testCase", required = true)
  Set<TestCaseData> testCaseDataSet;
  
  public String getProjectId() {
    return projectId;
  }
  
  public String getTestRunTemplateId() {
    return testRunTemplateId;
  }
  
  public String getTitle() {
    return title == null ? "" : title.trim();
  }
  
  public Integer getBaseline() {
    return baseline;
  }

  public boolean getKeepInHistory() {
    return keepInHistory != null && keepInHistory.booleanValue();
  }

  public Date getExecutionDate() {
    return executionDate;
  }

  public String getFormattedExecutionDate() {
    return new SimpleDateFormat("yyyyMMdd'-'HHmmss").format(executionDate);
  }

  public String getSystemUnderTestCategory() {
    return systemUnderTestCategory != null ? systemUnderTestCategory.trim() : "";
  }

  public String getSystemUnderTestURI() {
    return systemUnderTestURI != null ? systemUnderTestURI.trim() : "";
  }
  
  public String getDetailedResultsURI() {
    return detailedResultsURI != null ? detailedResultsURI.trim() : "";
  }

  public boolean hasSimplifiedConditions() {
    return hasSimplifiedConditions != null && hasSimplifiedConditions;
  }
  
  public String getEnvironmentDescription() {
    return environmentDescription == null ? "" : environmentDescription.trim();
  }

  public Set<TestCaseData> getTestCaseDataSet() {
    return testCaseDataSet;
  }

}
