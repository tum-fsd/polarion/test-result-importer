package com.tulrfsd.polarion.testresultimporter.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import com.polarion.core.util.logging.Logger;

public class Settings {
  /** The name of the servlet's html upload page */
  public static final String HTML_FILENAME;
  
  /** The name of the xml schema file */
  public static final String XML_SCHEMA_FILENAME;
  
  public static final String TEST_RUN_FIELD_ID_BASELINE;
  public static final String TEST_RUN_FIELD_ID_SYSTEM_UNDER_TEST_CATEGORY;
  public static final String TEST_RUN_FIELD_ID_SYSTEM_UNDER_TEST_URI;
  public static final String TEST_RUN_FIELD_ID_DETAILED_RESULTS_URI;
  public static final String TEST_RUN_FIELD_ID_ENVIRONMENT_DESCRIPTION;
  public static final String TEST_RUN_FIELD_ID_SIMPLIFIED_CONDITIONS;
  public static final String TEST_RECORD_FIELD_ID_EXTERNAL_TEST_CASE_URI;
  public static final String TEST_RESULT_ID_PASSED;
  public static final String TEST_RESULT_ID_PARTIAL;
  public static final String TEST_RESULT_ID_FAILED;
  public static final String TEST_RESULT_ID_BLOCKED;
  public static final String TEST_RUN_STATUS_ID_PASSED;
  public static final String TEST_RUN_STATUS_ID_FAILED;
  public static final boolean CREATE_SUMMARY_DEFECT;
  
  private Settings() {
    throw new IllegalStateException("Util Class");
  }
  
  static {
    Properties properties = new Properties();
    String propFileName = "config.properties";

    InputStream propFileStream =
        Settings.class.getClassLoader().getResourceAsStream(propFileName);

    if (propFileStream != null) {
      try {
        properties.load(propFileStream);
      } catch (IOException e) {
        Logger.getLogger(Settings.class).error("Failed to load configuration properties: ", e);
      }
    }

    HTML_FILENAME = properties.getProperty("HTML_FILENAME", "upload.html");
    XML_SCHEMA_FILENAME = properties.getProperty("XML_SCHEMA", "TestResultsSchema.xsd");
    TEST_RUN_FIELD_ID_BASELINE = properties.getProperty("TEST_RUN_FIELD_ID_BASELINE", "baseline");
    TEST_RUN_FIELD_ID_SYSTEM_UNDER_TEST_CATEGORY = properties.getProperty("TEST_RUN_FIELD_ID_SYSTEM_UNDER_TEST_CATEGORY", "system_under_test_category");
    TEST_RUN_FIELD_ID_SYSTEM_UNDER_TEST_URI = properties.getProperty("TEST_RUN_FIELD_ID_SYSTEM_UNDER_TEST_URI", "system_under_test_uri");
    TEST_RUN_FIELD_ID_DETAILED_RESULTS_URI = properties.getProperty("TEST_RUN_FIELD_ID_DETAILED_RESULTS_URI", "detailed_results_uri");
    TEST_RUN_FIELD_ID_ENVIRONMENT_DESCRIPTION = properties.getProperty("TEST_RUN_FIELD_ID_ENVIRONMENT_DESCRIPTION", "environment_description");
    TEST_RUN_FIELD_ID_SIMPLIFIED_CONDITIONS = properties.getProperty("TEST_RUN_FIELD_ID_SIMPLIFIED_CONDITIONS", "simplified_conditions");
    TEST_RECORD_FIELD_ID_EXTERNAL_TEST_CASE_URI = properties.getProperty("TEST_RECORD_FIELD_ID_EXTERNAL_TEST_CASE_URI", "external_test_case_uri");
    TEST_RESULT_ID_PASSED = properties.getProperty("TEST_RESULT_ID_PASSED", "passed");
    TEST_RESULT_ID_PARTIAL = properties.getProperty("TEST_RESULT_ID_PARTIAL", "partial");
    TEST_RESULT_ID_FAILED = properties.getProperty("TEST_RESULT_ID_FAILED", "failed");
    TEST_RESULT_ID_BLOCKED = properties.getProperty("TEST_RESULT_ID_BLOCKED", "blocked");
    TEST_RUN_STATUS_ID_PASSED = properties.getProperty("TEST_RUN_STATUS_ID_PASSED", "passed");
    TEST_RUN_STATUS_ID_FAILED = properties.getProperty("TEST_RUN_STATUS_ID_FAILED", "failed");
    CREATE_SUMMARY_DEFECT = Boolean.parseBoolean(properties.getProperty("CREATE_SUMMARY_DEFECT", "false"));
  }
}
