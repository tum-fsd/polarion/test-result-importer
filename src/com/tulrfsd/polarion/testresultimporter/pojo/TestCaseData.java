package com.tulrfsd.polarion.testresultimporter.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Nodes for a test case in the xml results file.
 */
@XmlRootElement(name = "testCase")
@XmlAccessorType(XmlAccessType.FIELD)
public class TestCaseData {

  @XmlElement(required = true)
  String workItemId;
  
  @XmlElement(required = true)
  int workItemRevision;
  
  @XmlElement(required = false)
  String externalTestCaseURI;

  @XmlElement(required = false)
  String summary;
  
  @XmlElement(required = false)
  String verdict;

  @XmlElement(required = false)
  TestResultEnum result;
  
  @XmlElement(required = false)
  TestResultEnum evalResult;
  
  public String getProjectId() {
    return workItemId.contains("/") ? workItemId.substring(0, workItemId.indexOf("/")) : "";
  }

  public String getWorkItemId() {
    return workItemId.contains("/") ? workItemId.substring(workItemId.lastIndexOf("/") + 1) : workItemId;
  }
  
  public String getWorkItemRevision() {
    return Integer.toString(workItemRevision);
  }
  
  public String getExternalTestCaseURI() {
    return externalTestCaseURI == null ? "" : externalTestCaseURI.trim();
  }

  public String getSummary() {
    return summary != null ? summary.trim() : getVerdict();
  }
  
  private String getVerdict() {
    return verdict != null ? verdict.trim() : "";
  }

  public TestResultEnum getResult() {
    return result != null ? result : getEvalResult();
  }
  
  private TestResultEnum getEvalResult() {
    return evalResult;
  }
}
