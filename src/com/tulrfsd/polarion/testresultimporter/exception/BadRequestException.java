package com.tulrfsd.polarion.testresultimporter.exception;

import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import com.polarion.core.util.logging.Logger;

public class BadRequestException extends RuntimeException {

  private static final long serialVersionUID = 1L;
  private final String message;

  /**
   * Sets the status for the response and writes the message via resp.getWriter(). This exception is
   * not supposed to be caught but to terminate the servlet.
   * 
   * @param resp The http response.
   * @param message The message to be displayed in the logs and returned in the http response.
   */
  public BadRequestException(HttpServletResponse resp, String message) {
    this.message = message;
    resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
    resp.setContentType("text/plain");
    try {
      resp.getWriter().print(message);
      resp.getWriter().flush();
      resp.getWriter().close();
    } catch (IOException e) {
      Logger.getLogger(getClass()).error(e);
    }
  }

  @Override
  public String getMessage() {
    return message;
  }

}
