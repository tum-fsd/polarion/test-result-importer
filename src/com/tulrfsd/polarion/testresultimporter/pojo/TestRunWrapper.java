package com.tulrfsd.polarion.testresultimporter.pojo;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "testRuns")
public class TestRunWrapper {

  @XmlElement(name = "testRun", required = true)
  List<TestRunData> testRuns;
  
  public List<TestRunData> getTestRuns() {
    return testRuns;
  }
}
