package com.tulrfsd.polarion.testresultimporter.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;
import javax.security.auth.Subject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBException;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.SchemaFactory;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.IOUtils;
import org.xml.sax.SAXException;
import com.polarion.platform.security.AuthenticationFailedException;
import com.tulrfsd.polarion.testresultimporter.exception.BadRequestException;
import com.tulrfsd.polarion.testresultimporter.pojo.FormData;

public class TestResultImporterServlet extends HttpServlet {

  private static final long serialVersionUID = 1L;

  /** Processes the uploaded file from the http request */
  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, BadRequestException, IOException {
    req.setCharacterEncoding("UTF-8");
    if (!req.getContentType().startsWith("multipart/form-data")) {
      throw new BadRequestException(resp, "The HTTP post request must be multipart form data.");
    }
    
    FormData formData = getFormData(req, resp);
    
    // login user and see if credentials are valid
    Subject userSubject;
    try {
      userSubject = TestResultImporter.getUserSubject(formData.getToken(), formData.getUsername(), formData.getPassword());
    } catch (AuthenticationFailedException e) {
      resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
      resp.getWriter().print(e.getMessage());
      return;
    }

    // validate xml against xsd
    try {
      InputStream xsdStream = TestResultImporterServlet.class.getClassLoader().getResourceAsStream(Settings.XML_SCHEMA_FILENAME);

      SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI)
          .newSchema(new StreamSource(xsdStream)).newValidator()
          .validate(new StreamSource(formData.getXmlFile().getInputStream()));
    } catch (IOException e) {
      throw new BadRequestException(resp, "Exception while trying to open the xml schema file.");
    } catch (SAXException e) {
      throw new BadRequestException(resp, "The provided xml file could not be validated against the xml schema file.");
    }

    // perform import
    try {
      resp.setContentType("text/plain");
      new TestResultImporter(formData.getXmlFile().getInputStream()).importAs(userSubject);
      resp.setStatus(HttpServletResponse.SC_OK);
      resp.getWriter().print("All test runs successfully processed.");
      resp.getWriter().flush();
      resp.getWriter().close();
    } catch (RuntimeException | JAXBException e) {
      throw new BadRequestException(resp, e.getMessage());
    } catch (IOException e) {
      throw new ServletException(e.getMessage());
    }
  }

  /** Provides the html form to upload the results file manually. */
  @Override
  protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
      throws ServletException, IOException {
    
    String fileName = "";
    if (Objects.equals("/" + Settings.XML_SCHEMA_FILENAME, req.getPathInfo())) {
      fileName = Settings.XML_SCHEMA_FILENAME;
      resp.setContentType("application/xml");
    } else {
      fileName = Settings.HTML_FILENAME;
      resp.setContentType("text/html");
    }
    resp.getWriter().print(IOUtils.toString(TestResultImporterServlet.class.getClassLoader().getResourceAsStream(fileName), StandardCharsets.UTF_8));
  }
  
  
  private FormData getFormData(HttpServletRequest req, HttpServletResponse resp) {
    List<FileItem> parts = null;
    FormData formData = new FormData();
    try {
      parts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(req);

    } catch (FileUploadException e) {
      throw new BadRequestException(resp, "The HTTP post request did not provide a proper file.");
    }

    for (FileItem file : parts) {
      switch (file.getFieldName()) {
        case "xmlFile":
          if (file.isFormField() || !(file.getContentType().equals("text/xml") || file.getContentType().equals("application/xml")) || file.getSize() == 0) {
            throw new BadRequestException(resp,"The HTTP post request must provide a non-empty xml file");
          }
          formData.setXmlFile(file);
          break;
        case "token":
          formData.setToken(file.getString());
          break;
        case "username":
          formData.setUsername(file.getString());
          break;
        case "password":
          formData.setPassword(file.getString());
          break;
        default:
          break;
      }
    }

    if (formData.getXmlFile() == null) {
      throw new BadRequestException(resp, "The HTTP post request must provide a single file upload.");
    }
    if (formData.getToken().isEmpty() && (formData.getUsername().isEmpty() || formData.getPassword().isEmpty())) {
      throw new BadRequestException(resp, "The HTTP post request must either provide an AccessToken or username/password to login.");
    }
    return formData;
  }
}