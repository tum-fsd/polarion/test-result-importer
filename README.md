## Introduction
This servlet extension provides a webapp at `<Polarion Base URL>/polarion/testresultimporter` that lets you upload test results generated from automatic (model-based) test cases. A tool that uses this extension is [tico](https://gitlab.com/tum-fsd/tico).

## License

Download and use of the extension is free. By downloading the extension, you accept the license terms and confirm to have a valid Polarion license issued by Siemens.

See [COPYRIGHT](COPYRIGHT), [LICENSE](LICENSE), and [DCO](DCO) for licensing details.

## Configuration
Refer to the Polarion Help for general questions regarding the Polarion Setup.
- Check the `config.properties` file in the subfolder `webapp/WEB-INF` and make sure that the configuration matches your project configuration. The properties file maps the element names of the xml file to the custom field names in Polarion.

## Extension Deployment
 1. Build the plugin with Eclipse or the tool of your choice
 2. Stop the Polarion server
 3. Go to `<Polarion INSTALL>/polarion/extensions`.
 4. If the subfolder `tulrfsd` does not yet exist, create it.
 5. In this subfolder, create the folder structure `eclipse/plugins`.
 6. Copy the plugin folder `com.tulrfsd.polarion.testresultimporter_<version_number>` in here.
 7. Remove any existing older versions of the same plugin.
 8. Delete the folder `<Polarion INSTALL>/data/workspace/.config`
 9. Start the Polarion Server

## Polarion Configuration
Make sure the user uploading the data has read access to the whole project and read/write access to the project's folder `.polarion/testing/testruns` in the SVN repository.

The `config.properties` file allows you to adapt the importer to your Polarion project configuration. All custom fields specified in the properties file are optional. However, if you use them, they need to be configured in the following way:

| Field ID                   | Field Type                                      |
|----------------------------|-------------------------------------------------|
| baseline                   | Integer                                         |
| system_under_test_category | Single- or multi-enum with defined enum options |
| system_under_test_uri      | String                                          |
| detailed_results_uri       | String                                          |
| environment_description    | Text or RichText                                |
| simplified_conditions      | Boolean                                         |
| external_test_case_uri     | String                                          |

If all tests passed, the test run status will be `Passed`, otherwise `Failed`.

## Test Result Import
- The XML file for the test result import must follow the XSD schema file provided in `webapp/WEB-INF` which is also accessible at `<Polarion Base URL>/polarion/testresultimporter/TestResultsSchema.xsd`.
- The referenced test cases must already exist in the project.
- Open the URL `<Polarion Base URL>/polarion/testresultimporter` and upload the file manually or perform an HTTP POST request.
- The servlet will return HTTP response codes and log messages depending on the outcome.

**HTTP Response Codes**
 - 200: Everything went well.
 - 400: The content of the uploaded file was wrong (e.g. Test Case does not exist, test run already imported)
 - 500: Internal Server error (e.g. wrong config setup or Polarion access rights)

## Requirements
- Polarion 2310 or later
- [TULRFSD Polarion Core Extension](../../../../com.tulrfsd.polarion.core). Check the release notes for the minimum required version of this extension.